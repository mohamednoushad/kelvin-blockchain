const mysql = require('mysql');
require('../settings');

const connection = mysql.createConnection({
    host: process.env.mysqlHost,
    user: process.env.mysqlUser,
    password: process.env.mysqlPassword,
    database: process.env.mysqlDb
});

module.exports = {
    createDBConnection: function () {
        return new Promise((resolve, reject) => {
            connection.connect(function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    query: function (query, params) {
        return new Promise((resolve, reject) => {
            connection.query(query, params, function (err, results, fields) {
                if (err) {
                    reject(err);
                } else {
                    resolve(results);
                }
            });
        });
    },

    escape: (data) => {
        return connection.escape(data);
    }
}