const DBService = require("./dbservice");
const walletService = require("./wallet");
const passwordHash = require("password-hash");



module.exports = {
    validateUserRegistration: async function (username) {
        console.log("here", username);

        try {
            let queryForUser = "SELECT username FROM users WHERE username = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: [username]
            });
            console.log(resultOnQuery);
            if (resultOnQuery.length) {
                return true;
            } else {
                return false;
            }
        } catch (err) {
            throw err;
        }
    },

    registerUser: async function (username, password) {

        try {

            const keyObject = await walletService.generateKeyObject(password);
            let addressGeneratedForUser = "0x" + keyObject.address;
            await walletService.exportToFile(keyObject);


            const hashedPassword = passwordHash.generate(password);
            console.log(hashedPassword);

            console.log(username, hashedPassword, addressGeneratedForUser);

            var data = {
                table: "users",
                columns: ["username", "password", "address"],
                values: [
                    [username, hashedPassword, addressGeneratedForUser]
                ]
            }

            await DBService.insertQuery(data);

        } catch (error) {

            console.log(error);

        }

    },

    loginUser : async function (username, password) {

        try {

            let queryForUser = "SELECT * FROM users WHERE username = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: [username]
            });

            console.log(resultOnQuery);
            if (resultOnQuery.length) {
                let userPasswordFromDb = resultOnQuery[0].password;

                const passWordCheck = passwordHash.verify(password,userPasswordFromDb)

                return {userLoginStatus:passWordCheck,userDetails:resultOnQuery[0]};
                
            } else {
                console.log("no user in db");
                return {userLoginStatus:passWordCheck,userDetails:null}
            }
            
        } catch (error) {

            console.log(error);
            
        }

    }
};