require("../settings");
const jwt = require('jsonwebtoken');

module.exports = {

    signPayload: function (payload) {
        return  jwt.sign({
            user: payload
        }, process.env.jwtSecret);
    },

    verifySignature: function (req, res, next) {

        const bearerHeader = req.headers['authorization'];

        if (typeof bearerHeader !== 'undefined') {

            const bearer = bearerHeader.split(' ');
            const bearerToken = bearer[1];
            req.token = bearerToken;
            next();

        } else {
            res.sendStatus(403);
        }

    },

    getPayload : function(token) {
        return jwt.verify(token, process.env.jwtSecret);
    }


}