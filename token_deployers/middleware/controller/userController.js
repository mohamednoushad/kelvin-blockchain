const express = require("express");
const router = express.Router();
const userHelper = require('../helpers/user');
const authentication = require('../authentication/index');

router.post("/register", async (req, res) => {

  let data = req.body;
  console.log("called register api");

  try {

    if (data.username && data.password) {
      let isUserExisting = await userHelper.validateUserRegistration(data.username);

      if (isUserExisting) {
        res.status(409).json({
          message: `a user with username present`
        })

      } else {
        await userHelper.registerUser(data.username, data.password);
        res.status(200).json({
          message: `user registered successfully`
        })
      }

    } else {
      res.status(400).json({
        message: `Body Should Contain Fields "username" and "password" `
      })
    }

  } catch (error) {

    console.log(error);
    res.status(400).json({
      message: error
    })
  }

});

router.post("/login", async (req, res) => {
  let data = req.body;
  console.log("called login api");

  try {

    if (data.username && data.password) {

      let isUserExisting = await userHelper.validateUserRegistration(data.username);

      if (isUserExisting) {

        const {
          userLoginStatus,
          userDetails
        } = await userHelper.loginUser(data.username, data.password);

        if (userLoginStatus) {

          const token = await authentication.signPayload(userDetails);

          res.status(200).json({
            token: token
          })


        } else {
          res.status(400).json({
            message: `invalid password`
          })

        }

      } else {

        res.status(404).json({
          message: `user not found`
        })

      }

    } else {
      res.status(400).json({
        message: `Body Should Contain Fields "username" and "password" `
      })
    }

  } catch (error) {

    console.log(error);
    res.status(400).json({
      message: error
    })

  }

});

// router.get("/list", (req, res) => {
//   let options = {};
//   options.limit = req.query.limit;
//   options.page = req.query.page;
//   let select = "number blockHash timestamp totalTransactions number";
//   Block.reversePaginate({}, options, select)
//     .then(blocks => {
//       return res.status(200).json({
//         blocks: blocks
//       });
//     })
//     .catch(err => {
//       return res.status(500).json({
//         error: err.message
//       });
//     });
// });

module.exports = router;