require("./settings");
const db = require("./database/connection");
const express = require("express");
const app = express();
const cors = require("cors");
const blockController = require("./controller/blockController");
const utilsController = require("./controller/utilsController");
const syncBlock = require("./sync/saveBlocks");
const mysql = require('./helpers/mysql');
const os = require('os');
const mkdirp = require('mkdirp');
const config = require('./config');


const bodyParser = require("body-parser");

const userController = require("./controller/userController");

// var prefix = os.homedir();
// console.log(prefix);
// const datadir = prefix + '/Documents/Miscellaneous/Kelvin/kelvin-blockchain/token_deployers/middleware/keystore';
// console.log(datadir);
app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: false
}));

// parse application/json
app.use(bodyParser.json());

app.use("/api/", blockController);
app.use("/api/utils/", utilsController);

app.use("/api/user/", userController);

app.get("/test", (req, res) => {
  res.json({
    data: "etho"
  });
});

async function initializeServer() {
  try {
    await mkdirp(await config.datadir());
    console.log('Directory Created');
    await mysql.createDBConnection()
  } catch (error) {
    console.log(error);
  }
}

initializeServer();



syncBlock();

app.listen(3000, () => {
  console.log("Server running on port 3000");
});



module.exports.app = app;