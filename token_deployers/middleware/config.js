const os = require('os');

module.exports = {

    datadir: async function () {
        var prefix = await os.homedir();
        return prefix + '/Documents/Miscellaneous/Kelvin/kelvin-blockchain/token_deployers/middleware/keystore';
    }
}