import React from 'react';
import { Link } from 'react-router-dom';

import DetailView from '../components/DetailView';

import getMainUrl from '../utils/getMainUrl';
import { backendAPIs } from '../strings/api';

import { my8n } from '../services/localizationService';

function DetailViewPage(props) {
	const id = props.match.params.id;
	const url = getMainUrl(props);
	let api,
		title,
		columns = [];

	if (url === 'block') {
		api = backendAPIs.GETBLOCK + id;
		title = my8n('Block');
		columns = [
			{
				name: my8n('Block_Height'),
				key: 'number'
			},
			{
				name: my8n('Hash'),
				key: 'blockHash'
			},
			{
				name: my8n('Timestamp'),
				key: 'timestamp'
			},
			{
				name: my8n('Total_Transactions'),
				key: 'totalTransactions'
			},
			{
				name: my8n('Difficulty'),
				key: 'difficulty'
			},
			{
				name: my8n('Total_Difficulty'),
				key: 'totalDifficulty'
			},
			{
				name: my8n('Sha3Uncles'),
				key: 'sha3Uncles'
			}
		];
	} else {
		api = backendAPIs.GETTRANSACTION + id;
		title = my8n('Transaction');
		columns = [
			{
				name: my8n('Block_Hash'),
				key: 'blockHash'
			},
			{
				name: my8n('Number'),
				key: 'blockNumber',
				render: blockNumber => (
					<Link to={`/block/${blockNumber}`}>{blockNumber}</Link>
				)
			},
			{
				name: my8n('Hash'),
				key: 'transactionHash'
			},
			{
				name: my8n('Nonce'),
				key: 'nonce'
			},
			{
				name: my8n('From'),
				key: 'from',
				render: fromAddress => (
					<Link to={`/address/${fromAddress}`}>{fromAddress}</Link>
				)
			},
			{
				name: my8n('To'),
				key: 'to',
				render: toAddress => (
					<Link to={`/address/${toAddress}`}>{toAddress}</Link>
				)
			},
			{
				name: my8n('Timestamp'),
				key: 'timestamp'
			}
		];
	}
	return <DetailView title={title} api={api} columns={columns} id={id} />;
}

export default DetailViewPage;
