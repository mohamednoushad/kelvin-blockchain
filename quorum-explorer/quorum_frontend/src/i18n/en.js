export default {
    SITE__TITLE: 'BBAI Chain Scan',

    FOOTER__HEADER: 'Powered By BBAI',
    FOOTER__SUBHEADER: 'BBAI Chain Scan is a Block Explorer for BBAI Block Chain, a decentralized smart contracts platform.',
    FOOTER__COMPANY: 'Company',
    FOOTER__COMPANY__LINK__ONE: 'https://link1',
    FOOTER__COMPANY__LINK__TWO: 'https://link1',
    FOOTER__COMPANY__LINK__THREE: 'https://link1',
    FOOTER__SOCIAL: 'Social',
    FOOTER__SOCIAL__LINK__ONE: 'https://link1',
    FOOTER__SOCIAL__LINK__TWO: 'https://link1',
    FOOTER__SOCIAL__LINK__THREE: 'https://link1',

    DETAILLISTHEADER__TX: 'Last 500k transactions shown',

    LIST__BUTTON: 'View All',

    LISTITEM__FROM: 'From',
    LISTITEM__TO: 'To',
    LISTITEM__BLOCK__TITLE: 'Bk',
    LISTITEM__TX__TITLE: 'Tx',
    LISTITEM__TXNS: 'txns',

    DASHBOARD__BLOCK: 'LATEST BLOCK',
    DASHBOARD__TRANSACTIONS: 'TRANSACTIONS',
    DASHBOARD__CONTRACTS: 'CONTRACTS',

    SEARCH__PLACEHOLDER: 'Search by Address / Txn Hash / Block',

    PAGINATION__FIRST: 'First',
    PAGINATION__LAST: 'Last',
    PAGINATION__PAGE: 'Page',
    PAGINATION__PAGE_OF: 'of',
    Blocks: 'Blocks',
    Transactions: 'Transactions',
    days: ' days ',
    hours: ' hours ',
    mins: ' mins ',
    secs: ' secs ',
    Block: 'Block',
    Block_Height: 'Block Height',
    Hash: 'Hash',
    Timestamp: 'Timestamp',
    Total_Transactions: 'Total Transactions',
    Difficulty: 'Difficulty',
    Total_Difficulty: 'Total Difficulty',
    Sha3Uncles: 'Sha3Uncles',
    Transaction: 'Transaction',
    Block_Hash: 'Block Hash',
    Number: 'Number',
    Nonce: 'Nonce',
    From: 'From',
    To: 'To',
    Age: 'Age',
    Address: 'Address',
    Txn_Hash: 'Txn Hash'


};